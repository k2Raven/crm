import locale
from datetime import datetime, date
from calendar import HTMLCalendar, day_abbr
from webapp.models import Lesson
from django.urls import reverse


class _localized_month:
    locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')
    _months = [date(2001, i + 1, 1).strftime for i in range(12)]
    _months.insert(0, lambda x: "")

    def __init__(self, format):
        self.format = format

    def __getitem__(self, i):
        funcs = self._months[i]
        if isinstance(i, slice):
            return [f(self.format) for f in funcs]
        else:
            return funcs(self.format)

    def __len__(self):
        return 13


def get_paginate_day_html(themonth, theyear):
    before_month = f'{theyear}-{themonth - 1}'
    next_month = f'{theyear}-{themonth + 1}'
    if themonth == 1:
        before_month = f'{theyear - 1}-12'
    elif themonth == 12:
        next_month = f'{theyear + 1}-1'

    d = datetime.today()
    now_month = f'{d.year}-{d.month}'
    is_disabled = "disabled" if d.year == theyear and d.month == themonth else ""
    a_now_str = f'<a href="{reverse("webapp:index")}?day={now_month}" ' \
                f'class="btn btn-outline-secondary ms-3 {is_disabled}" >Сегодня</a>'

    return f'<a href="{reverse("webapp:index")}?day={before_month}" ' \
           f'class="btn btn-outline-secondary rounded-circle"><i class="bi bi-chevron-double-left"></i></a>' \
           f'<a href="{reverse("webapp:index")}?day={next_month}" ' \
           f'class="btn btn-outline-secondary rounded-circle ms-2">' \
           f'<i class="bi bi-chevron-double-right"></i></a>{a_now_str}'


month_name = _localized_month('%B')


class Calendar(HTMLCalendar):
    def __init__(self, year=None, month=None, event_queryset=None, user_not_student=True):
        self.year = year
        self.month = month
        if not event_queryset:
            event_queryset = Lesson.objects.all()
        self.event_queryset = event_queryset
        self.user_not_student = user_not_student
        super(Calendar, self).__init__()

    def formatday(self, day, events, bg_b='bg-white'):
        d = datetime.today()
        d_now = d.year == self.year and d.month == self.month and d.day == day

        events_per_day = events.filter(data_start__day=day)
        d = ''
        for event in events_per_day:
            d += f'<a class="rounded-pill calendar-week-date-event text-truncate" ' \
                 f'href="{reverse("webapp:lesson_view", kwargs={"lesson_pk": event.pk})}" ' \
                 f'style="background-color: {event.period.group.color};">' \
                 f'{event.number}. {event.get_type_lesson_display()}' \
                 f'</a>'

        if day != 0:
            return f"<div class='calendar-week-date {'alert-success' if d_now else bg_b}'>" \
                   f"<span class='calendar-week-date-span d-inline-block  {'fs-1' if d_now else 'fs-5'} fw-bolder' >{day}</span>" \
                   f"<div class='calendar-week-date-events overflow-auto  fs-6 d-flex flex-column' " \
                   f"style='{'margin-top: -4px' if d_now else ''}'>{d}</div>" \
                   f"</div>"
        return '<div></div>'

    def formatmonthname(self, theyear, themonth, withyear=True):
        if withyear:
            s = '%s %s' % (month_name[themonth], theyear)
        else:
            s = '%s' % month_name[themonth]
        url_create_lesson = f'<a class="btn btn-outline-success rounded-circle ms-3" href="{reverse("webapp:lesson_create")}">' \
                            f'<i class="bi bi-plus-lg"></i>' \
                            f'</a>'
        return f'<div class="position-relative m-0 bg-white pt-4 pb-3" >' \
               f'<p class="calendar-month text-center fs-2 fw-bolder">{s}</p>' \
               f'<div class="position-absolute top-50 end-0 translate-middle-y pe-5">' \
               f'{get_paginate_day_html(themonth, theyear)}' \
               f'{url_create_lesson if self.user_not_student else ""}' \
               f'</div>' \
               f'</div>'

    def formatweek(self, theweek, events):
        week = ''
        bg_b = 'bg-white'
        for d, weekday in theweek:
            week += self.formatday(d, events, bg_b)
            if bg_b == 'bg-white':
                bg_b = 'bg-light'
            else:
                bg_b = 'bg-white'
        return f'<div class="calendar-week border-bottom border-3 border-dark d-flex"> {week} </div>'

    def formatweekday(self, day, bg_b='bg-white'):

        return f'<span class="calendar-week-day-name text-center inline-block fs-4 fw-bolder {bg_b} {self.cssclasses_weekday_head[day]}">' \
               f'{day_abbr[day]}' \
               f'</span>'

    def formatweekheader(self):
        s = ''
        bg_b = 'bg-white'
        for i in self.iterweekdays():
            s += self.formatweekday(i, bg_b)
            if bg_b == 'bg-white':
                bg_b = 'bg-light'
            else:
                bg_b = 'bg-white'
        return '<div class="calendar-week-name d-flex border-bottom border-3 border-dark">%s</div>' % s

    def formatmonth(self, withyear=True):
        if self.year == datetime.today().year:
            withyear = False
        events = self.event_queryset.filter(data_start__year=self.year, data_start__month=self.month)

        cal = f'<div class="calendar d-flex flex-column rounded  bg-light">\n'
        cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
        cal += f'{self.formatweekheader()}\n'
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f'{self.formatweek(week, events)}\n'
        cal += '</div>'
        return cal
