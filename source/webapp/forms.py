from django import forms
from webapp.models import Period, Lesson, Group, Language, Material, LinkBroadcast, Work
from django.forms import ValidationError, widgets


class WorkUserForm(forms.ModelForm):
    class Meta:
        model = Work
        fields = ['work', 'link']
        widgets = {
            'work': widgets.FileInput(attrs={
                'class': 'form-control rounded-pill mb-3'
            }),
            'link': widgets.URLInput(attrs={
                'class': 'form-control rounded-pill mb-3'
            })
        }


class WorkCheckForm(forms.ModelForm):
    class Meta:
        model = Work
        fields = ['grade', 'status', 'review']
        widgets = {
            'grade': widgets.NumberInput(attrs={
                'class': 'form-control mb-3'
            }),
            'review': forms.Textarea(attrs={
                'id': 'basic-example',
            }),
            'status': forms.CheckboxInput(attrs={
                'class': 'mb-3'
            })
        }


class LinkBroadcastForm(forms.ModelForm):
    class Meta:
        model = LinkBroadcast
        fields = ['link']


class LanguageForm(forms.ModelForm):
    class Meta:
        model = Language
        fields = ['name_language']
        widgets = {
            'name_language': widgets.TextInput(attrs={
                'class': 'form-control w-50'
            })
        }


class PeriodForm(forms.ModelForm):
    class Meta:
        model = Period
        fields = ['data_start', 'data_end']
        widgets = {
            'data_start': forms.DateInput(attrs={
                'type': 'date',
                'class': 'form-control w-50'
            }),
            'data_end': forms.DateInput(attrs={
                'type': 'date',
                'class': 'form-control w-50'
            }),
        }

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['data_start'] > cleaned_data['data_end']:
            raise ValidationError("Дата начала периода не может быть больше даты окончания периода")

        return cleaned_data


class LessonForm(forms.ModelForm):
    group = forms.ModelChoiceField(queryset=None, label='Группа',
                                   widget=forms.Select(attrs={'class': 'form-select w-50'}))

    def __init__(self, *args, filter_group_users=None, **kwargs):
        super().__init__(*args, **kwargs)
        if filter_group_users:
            self.fields["group"].queryset = Group.objects.filter(users__in=filter_group_users).order_by('language')
        else:
            self.fields["group"].queryset = Group.objects.order_by('language')

    class Meta:
        model = Lesson
        fields = ['group', 'theme', 'data_start', 'type_lesson']
        widgets = {
            'data_start': forms.DateTimeInput(attrs={
                'type': 'datetime-local',
                'class': 'form-control w-50'
            }),
            'theme': widgets.TextInput(attrs={
                'class': 'form-control w-50'
            }),
            'type_lesson': forms.Select(attrs={
                'class': 'form-select w-50'
            })

        }


class LessonGroupForm(forms.ModelForm):
    class Meta:
        model = Lesson
        fields = ['theme', 'data_start', 'type_lesson']
        widgets = {
            'data_start': forms.DateTimeInput(attrs={
                'type': 'datetime-local',
                'class': 'form-control w-50'
            }, format='%Y-%m-%dT%H:%M:%S'),
            'theme': widgets.TextInput(attrs={
                'class': 'form-control w-50'
            }),
            'type_lesson': forms.Select(attrs={
                'class': 'form-select w-50'
            })
        }


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['number_group', 'data_start', 'color', 'data_end']

        widgets = {
            'number_group': widgets.NumberInput(attrs={
                'class': 'form-control w-50',
                'value': 1,
            }),
            'data_start': forms.DateInput(attrs={
                'type': 'date',
                'class': 'form-control w-50'
            }, format='%Y-%m-%d'),
            'color': widgets.Input(attrs={
                'class': 'form-control form-control-color',
                'value': "#563d7c",
                'type': "color",
            }),
            'data_end': forms.DateInput(attrs={
                'type': 'date',
                'class': 'form-control w-50'
            }, format='%Y-%m-%d'),
        }


class MaterialForm(forms.ModelForm):
    class Meta:
        model = Material
        fields = ['title', 'content']

        widgets = {
            'title': widgets.TextInput(attrs={
                'class': 'form-control w-50',
            }),
            'content': forms.Textarea(attrs={
                'id': 'basic-example'
            }),
        }


class SimpleSearchForm(forms.Form):
    search = forms.CharField(max_length=50, required=False, label='Найти',
                             widget=widgets.TextInput(attrs={'class': "form-control w-25"}))


class MaterialSearchForm(forms.Form):
    def __init__(self, *args, search=None, material_type=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['materials'].queryset = Material.objects.filter(type_material=material_type)
        if search:
            self.fields['materials'].queryset = self.fields['materials'].queryset.filter(title__icontains=search)

    materials = forms.ModelChoiceField(queryset=None, label='материалы', widget=widgets.RadioSelect())
