from django.contrib.auth import get_user_model
from django.db import models


class Language(models.Model):
    name_language = models.CharField(max_length=200, unique=True, verbose_name='Название языка')

    def __str__(self):
        return "{}. {}".format(self.pk, self.name_language)


class Group(models.Model):
    number_group = models.IntegerField(verbose_name='Номер группы')
    data_start = models.DateField(verbose_name='Дата запуска группы')
    color = models.CharField(max_length=20, verbose_name="Цвет группы")
    data_end = models.DateField(null=True, blank=True, verbose_name='Дата выпуска группы')
    language = models.ForeignKey('webapp.Language', related_name='groups', on_delete=models.PROTECT,
                                 verbose_name='Язык')
    users = models.ManyToManyField(get_user_model(), related_name='groups_language', through='webapp.GroupUser',
                                   through_fields=('group', 'user'), blank=True)
    is_activation = models.BooleanField(default=True)

    def __str__(self):
        return "{}№{} ({}-{}) ".format(self.language.name_language, self.number_group, self.data_start, self.data_end)


class Period(models.Model):
    number_period = models.IntegerField(verbose_name='Номер периода')
    data_start = models.DateField(verbose_name='Дата начала периода')
    data_end = models.DateField(verbose_name='Дата окончания периода')
    group = models.ForeignKey('webapp.Group', related_name='periods', on_delete=models.PROTECT,
                              verbose_name='Группа')

    def __str__(self):
        return "{}. {} #{}  ({}-{})".format(self.pk, self.group, self.number_period, self.data_start, self.data_end)


TYPE_TASK = 'task'
TYPE_MATERIAL = 'material'
TYPES_MATERIAL = [
    (TYPE_TASK, 'Задание'),
    (TYPE_MATERIAL, 'Материал'),
]


class Material(models.Model):
    title = models.CharField(max_length=200, verbose_name='Заголовок')
    content = models.TextField(null=True, blank=True, verbose_name='Содержимое')
    group = models.ForeignKey('webapp.Group', related_name='material', on_delete=models.PROTECT,
                              verbose_name='Группа')
    lesson = models.ForeignKey('webapp.Lesson', on_delete=models.PROTECT, related_name='materials', verbose_name='Урок')
    type_material = models.CharField(choices=TYPES_MATERIAL, max_length=20, verbose_name='Тип')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Время создания")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Время изменения")

    def __str__(self):
        return "{}. {}".format(self.pk, self.title)


TYPE_LESSON = 'lesson'
TYPE_WEBINAR = 'webinar'
TYPE_CONTROL = 'control'
CHOICES_TYPES_LESSON = ((TYPE_LESSON, 'Занятие'), (TYPE_WEBINAR, 'Вебинар'), (TYPE_CONTROL, 'Контрольная'))


class Lesson(models.Model):
    number = models.IntegerField(default=1, verbose_name='Номер урока')
    data_start = models.DateTimeField(verbose_name='Время начала урока')
    theme = models.CharField(max_length=200, null=True, blank=True, verbose_name='Тема урока')
    deadline_task = models.DateTimeField(null=True, blank=True, verbose_name='Дедлайн задания')
    type_lesson = models.CharField(choices=CHOICES_TYPES_LESSON, null=True, verbose_name='Тип урока')
    period = models.ForeignKey('webapp.Period', related_name='lessons', on_delete=models.PROTECT,
                               verbose_name='Период')

    def __str__(self):
        return "{}. {} ({})".format(self.pk, self.theme, self.data_start)

    def get_razdatka(self):
        return self.materials.filter(type_material=TYPE_MATERIAL).order_by('-created_at').first()

    def get_task(self):
        return self.materials.filter(type_material=TYPE_TASK).order_by('-created_at').first()


class LinkBroadcast(models.Model):
    link = models.CharField(max_length=200, verbose_name='Ссылка')
    lesson = models.ForeignKey('webapp.Lesson', related_name='links_broadcast', on_delete=models.PROTECT,
                               verbose_name='Урок')

    def __str__(self):
        return "{}. {}".format(self.pk, self.link)


class Work(models.Model):
    grade = models.FloatField(verbose_name='Оценка', default=0)
    work = models.FileField(null=True, blank=True, verbose_name='Файл')
    link = models.CharField(max_length=200, null=True, blank=True, verbose_name='Ссылка')
    review = models.TextField(null=True, blank=True, verbose_name='Рецензия')
    lesson = models.ForeignKey('webapp.Lesson', related_name='lesson_works', on_delete=models.CASCADE,
                               verbose_name='Урок')
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='user_works',
                             verbose_name='Пользователь')
    status = models.BooleanField(default=False, verbose_name='Проверена')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Время загрузки")


class GroupUser(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='user_groups',
                             verbose_name='Пользователь')
    group = models.ForeignKey('webapp.Group', related_name='group_users', on_delete=models.PROTECT,
                              verbose_name='Группа')
    data_start = models.DateField(verbose_name='Дата начала учебы')
    data_end = models.DateField(null=True, blank=True, verbose_name='Дата окончания учебы')
