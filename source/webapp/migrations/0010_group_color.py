# Generated by Django 4.2.1 on 2023-05-29 13:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0009_alter_lesson_razdatka_alter_lesson_task'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='color',
            field=models.CharField(default='#', verbose_name='Цвет группы'),
            preserve_default=False,
        ),
    ]
