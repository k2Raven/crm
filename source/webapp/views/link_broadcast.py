from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import CreateView, DeleteView
from webapp.models import LinkBroadcast, Lesson
from webapp.forms import LinkBroadcastForm
from django.shortcuts import get_object_or_404
from django.urls import reverse


class LinkBroadcastCreate(PermissionRequiredMixin, CreateView):
    model = LinkBroadcast
    form_class = LinkBroadcastForm

    def has_permission(self):
        group = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk')).period.group
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in group.users.all()

    def form_valid(self, form):
        lesson = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk'))
        form.instance.lesson = lesson

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('webapp:lesson_view', kwargs={'lesson_pk': self.object.lesson.pk})


class LinkBroadcastDelete(PermissionRequiredMixin, DeleteView):
    model = LinkBroadcast
    pk_url_kwarg = "link_broadcast_pk"

    def has_permission(self):
        group = get_object_or_404(LinkBroadcast, pk=self.kwargs.get('link_broadcast_pk')).lesson.period.group
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in group.users.all()

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('webapp:lesson_view', kwargs={'lesson_pk': self.object.lesson.pk})
