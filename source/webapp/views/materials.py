from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView, FormView
from webapp.models import Material, Group, Lesson
from webapp.forms import MaterialForm, MaterialSearchForm, SimpleSearchForm
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy, reverse
from django.utils.http import urlencode


class MaterialList(LoginRequiredMixin, ListView):
    template_name = 'materials/list.html'
    model = Material
    context_object_name = 'materials'
    paginate_by = 20

    def get_queryset(self):
        queryset = super().get_queryset()
        if not self.request.user.is_superuser and not self.request.user.is_admin():
            queryset = queryset.filter(group__users=self.request.user)
        return queryset


class MaterialView(PermissionRequiredMixin, DetailView):
    template_name = 'materials/view.html'
    model = Material
    pk_url_kwarg = 'material_pk'

    def has_permission(self):
        users = get_object_or_404(Material, pk=self.kwargs.get('material_pk')).group.users.all()
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user in users


class MaterialCreate(PermissionRequiredMixin, CreateView):
    template_name = 'materials/create.html'
    model = Material
    form_class = MaterialForm

    def has_permission(self):
        users = get_object_or_404(Group, pk=self.kwargs.get('group_pk')).users.all()
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in users

    def form_valid(self, form):
        group = get_object_or_404(Group, pk=self.kwargs.get('group_pk'))
        form.instance.group = group
        lesson = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk'))
        form.instance.lesson = lesson
        form.instance.type_material = self.kwargs.get('type')

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('webapp:lesson_view', kwargs={'lesson_pk': self.object.lesson.pk})


class MaterialSearchCreate(PermissionRequiredMixin, FormView):
    template_name = 'materials/search_create.html'
    model = Material
    form_class = MaterialSearchForm

    def has_permission(self):
        users = get_object_or_404(Group, pk=self.kwargs.get('group_pk')).users.all()
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in users

    def dispatch(self, request, *args, **kwargs):
        self.search_form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        kwargs.update({"material_type": self.kwargs.get('type')})
        if self.search_value:
            kwargs.update({"search": self.search_value})
        return kwargs

    def get_search_form(self):
        return SimpleSearchForm(self.request.GET)

    def get_search_value(self):
        if self.search_form.is_valid():
            return self.search_form.cleaned_data['search']

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['search_form'] = self.search_form
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
            context['search'] = self.search_value
        return context

    def form_valid(self, form):
        material = form.cleaned_data['materials']
        material.pk = None
        group = get_object_or_404(Group, pk=self.kwargs.get('group_pk'))
        material.group = group
        lesson = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk'))
        material.lesson = lesson
        material.save()
        self.object = material
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('webapp:lesson_view', kwargs={'lesson_pk': self.object.lesson.pk})


class MaterialUpdate(PermissionRequiredMixin, UpdateView):
    template_name = 'materials/create.html'
    model = Material
    form_class = MaterialForm
    pk_url_kwarg = 'material_pk'

    def get_success_url(self):
        return reverse('webapp:lesson_view', kwargs={'lesson_pk': self.object.lesson.pk})

    def has_permission(self):
        users = get_object_or_404(Material, pk=self.kwargs.get('material_pk')).group.users.all()
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in users


class MaterialDelete(PermissionRequiredMixin, DeleteView):
    model = Material
    pk_url_kwarg = "material_pk"
    success_url = reverse_lazy('webapp:materials_list')

    def has_permission(self):
        users = get_object_or_404(Material, pk=self.kwargs.get('material_pk')).group.users.all()
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in users

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)
