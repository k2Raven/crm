import datetime

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView
from webapp.models import Group, Language
from webapp.forms import GroupForm
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404


class GroupsList(LoginRequiredMixin, ListView):
    template_name = 'groups/list.html'
    model = Language
    context_object_name = 'languages'

    def get_queryset(self):
        queryset = super().get_queryset()
        if not self.request.user.is_superuser and not self.request.user.is_admin():
            queryset = queryset.filter(groups__users=self.request.user).distinct()
        return queryset


class GroupView(PermissionRequiredMixin, DetailView):
    template_name = 'groups/view.html'
    model = Group
    pk_url_kwarg = "group_pk"

    def has_permission(self):
        group = get_object_or_404(Group, pk=self.kwargs.get('group_pk'))
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user in group.users.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        group = self.object
        periods = group.periods.order_by('data_end')
        context['periods'] = periods
        now = datetime.datetime.now()
        context['period_now'] = None
        context['now'] = now
        period_pk = self.kwargs.get('period_pk', '')
        if period_pk:
            try:
                context['period_now'] = periods.get(pk=period_pk)
            except ObjectDoesNotExist:
                raise Http404()
        else:
            period_now = periods.filter(data_start__lte=now, data_end__gte=now)
            context['period_now'] = periods.first()
            if period_now.count():
                context['period_now'] = period_now.first()
        if context['period_now']:
            context['lessons'] = context['period_now'].lessons.order_by('data_start')
        return context


class GroupCreate(PermissionRequiredMixin, CreateView):
    template_name = 'groups/create.html'
    model = Group
    form_class = GroupForm
    success_url = reverse_lazy('webapp:groups_list')
    pk_url_kwarg = 'language_pk'

    def has_permission(self):
        return self.request.user.is_superuser or self.request.user.is_admin()

    def form_valid(self, form):
        language = get_object_or_404(Language, pk=self.kwargs.get('language_pk'))
        form.instance.language = language
        return super().form_valid(form)


class GroupUpdate(PermissionRequiredMixin, UpdateView):
    template_name = 'groups/update.html'
    model = Group
    form_class = GroupForm
    success_url = reverse_lazy('webapp:groups_list')
    pk_url_kwarg = 'group_pk'

    def has_permission(self):
        return self.request.user.is_superuser or self.request.user.is_admin()


class GroupDelete(PermissionRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy('webapp:groups_list')
    pk_url_kwarg = 'group_pk'

    def has_permission(self):
        return self.request.user.is_superuser or self.request.user.is_admin()

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)
