from django.views.generic import DetailView, CreateView, UpdateView, DeleteView, TemplateView
from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.utils.safestring import mark_safe
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from webapp.utils import Calendar
from webapp.forms import LessonForm, LessonGroupForm, WorkUserForm
from webapp.models import Lesson, Period, Group, TYPE_CONTROL

from datetime import datetime, date


def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split('-'))
        return date(year, month, day=1)
    return datetime.today()


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        d = get_date(self.request.GET.get('day', None))
        event_queryset = None
        if self.request.user.is_admin():
            event_queryset = Lesson.objects.all()
        else:
            event_queryset = Lesson.objects.filter(period__group__users=self.request.user)
        cal = Calendar(d.year, d.month, event_queryset,
                       self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher())

        html_cal = cal.formatmonth(withyear=True)

        context['calendar'] = mark_safe(html_cal)
        return context


class LessonView(PermissionRequiredMixin, DetailView):
    template_name = 'lessons/view.html'
    model = Lesson
    pk_url_kwarg = "lesson_pk"

    def has_permission(self):
        lesson = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk'))
        return self.request.user.is_superuser or self.request.user.is_admin() \
            or self.request.user in lesson.period.group.users.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_work'] = self.object.lesson_works.filter(user=self.request.user).first()
        context['form'] = WorkUserForm()
        context['works_count'] = self.object.lesson_works.filter(status=False).count()
        if context['user_work']:
            context['form'] = WorkUserForm(instance=context['user_work'])
        return context


class LessonCreate(PermissionRequiredMixin, CreateView):
    template_name = 'lessons/create.html'
    model = Lesson
    form_class = LessonForm

    def has_permission(self):
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher()

    def get_success_url(self):
        return self.request.GET.get('next_url', reverse('webapp:index'))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.user.is_teacher():
            kwargs.update({"filter_group_users": [self.request.user]})
        return kwargs

    def form_valid(self, form):
        period = Period.objects.filter(data_start__lte=form.cleaned_data['data_start'],
                                       data_end__gte=form.cleaned_data['data_start'],
                                       group=form.cleaned_data['group']).first()
        form.instance.period = period
        last_lesson = Lesson.objects.filter(type_lesson=form.cleaned_data['type_lesson'],
                                            data_start__lte=form.cleaned_data['data_start'],
                                            period__in=form.cleaned_data['group'].periods.all()).order_by(
            'number').last()
        number_lesson = 1
        if last_lesson:
            number_lesson += last_lesson.number

        form.instance.number = number_lesson

        return super().form_valid(form)


class LessonGroupCreate(PermissionRequiredMixin, CreateView):
    template_name = 'lessons/create.html'
    model = Lesson
    form_class = LessonGroupForm

    def has_permission(self):
        group = get_object_or_404(Group, pk=self.kwargs.get('group_pk'))
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in group.users.all()

    def get_success_url(self):
        return reverse('webapp:group_view_period_pk',
                       kwargs={'group_pk': self.object.period.group.pk, 'period_pk': self.object.period.pk})

    def form_valid(self, form):
        group = get_object_or_404(Group, pk=self.kwargs.get('group_pk'))
        period = Period.objects.filter(data_start__lte=form.cleaned_data['data_start'],
                                       data_end__gte=form.cleaned_data['data_start'],
                                       group=group).first()
        form.instance.group = group
        form.instance.period = period
        if form.cleaned_data['type_lesson'] != TYPE_CONTROL:
            last_lesson = Lesson.objects.filter(type_lesson=form.cleaned_data['type_lesson'],
                                                data_start__lte=form.cleaned_data['data_start'],
                                                period__in=group.periods.all()).order_by(
                'number').last()
        else:
            last_lesson = Lesson.objects.filter(type_lesson=form.cleaned_data['type_lesson'],
                                                data_start__lte=form.cleaned_data['data_start']).order_by(
                'number').last()
        number_lesson = 1
        if last_lesson:
            number_lesson += last_lesson.number

        form.instance.number = number_lesson

        return super().form_valid(form)


class LessonUpdate(PermissionRequiredMixin, UpdateView):
    template_name = 'lessons/create.html'
    model = Lesson
    form_class = LessonGroupForm
    pk_url_kwarg = "lesson_pk"

    def has_permission(self):
        group = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk')).period.group
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in group.users.all()

    def get_success_url(self):
        return self.request.GET.get('next_url', reverse('webapp:lesson_view', kwargs={'lesson_pk': self.object.pk}))


class LessonUpdateDeadlineTask(PermissionRequiredMixin, UpdateView):
    model = Lesson
    pk_url_kwarg = "lesson_pk"
    fields = ['deadline_task']

    def has_permission(self):
        group = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk')).period.group
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in group.users.all()

    def get_success_url(self):
        return self.request.GET.get('next_url', reverse('webapp:lesson_view', kwargs={'lesson_pk': self.object.pk}))


class LessonDelete(PermissionRequiredMixin, DeleteView):
    model = Lesson
    pk_url_kwarg = "lesson_pk"

    def has_permission(self):
        group = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk')).period.group
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in group.users.all()

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.GET.get('next_url', reverse('webapp:index'))
