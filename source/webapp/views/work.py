from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import CreateView, UpdateView, ListView, DetailView
from webapp.models import Lesson, Work
from webapp.forms import WorkUserForm, WorkCheckForm
from django.shortcuts import get_object_or_404
from django.urls import reverse


class WorkView(PermissionRequiredMixin, DetailView):
    template_name = 'work/view.html'
    model = Work

    def has_permission(self):
        users = get_object_or_404(Work, pk=self.kwargs.get('pk')).lesson.period.group.users.all()
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in users

    def get_queryset(self):
        return Work.objects.filter(status=True)


class WorkUserCreate(PermissionRequiredMixin, CreateView):
    model = Work
    form_class = WorkUserForm
    template_name = 'work/create.html'

    def has_permission(self):
        users = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk')).period.group.users.all()
        return self.request.user in users

    def form_valid(self, form):
        lesson = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk'))
        form.instance.lesson = lesson
        form.instance.user = self.request.user

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('webapp:lesson_view', kwargs={'lesson_pk': self.object.lesson.pk})


class WorkUserUpdate(PermissionRequiredMixin, UpdateView):
    model = Work
    form_class = WorkUserForm
    pk_url_kwarg = 'work_pk'
    template_name = 'work/update.html'

    def has_permission(self):
        return self.request.user == get_object_or_404(Work, pk=self.kwargs.get('work_pk')).user

    def get_success_url(self):
        return reverse('webapp:lesson_view', kwargs={'lesson_pk': self.object.lesson.pk})


class WorkLessonCheck(PermissionRequiredMixin, ListView):
    model = Work
    template_name = 'work/lesson_check.html'
    context_object_name = 'works'

    def has_permission(self):
        users = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk')).period.group.users.all()
        return self.request.user.is_teacher() and self.request.user in users

    def get_queryset(self):
        lesson = get_object_or_404(Lesson, pk=self.kwargs.get('lesson_pk'))
        queryset = super().get_queryset().filter(lesson=lesson)
        return queryset


class WorkUserCheck(PermissionRequiredMixin, UpdateView):
    model = Work
    form_class = WorkCheckForm
    pk_url_kwarg = 'work_pk'
    template_name = 'work/check.html'

    def has_permission(self):
        users = get_object_or_404(Work, pk=self.kwargs.get('pk')).lesson.period.group.users.all()
        return self.request.user.is_teacher() and self.request.user in users

    def get_success_url(self):
        return reverse('webapp:work_lesson_check', kwargs={'lesson_pk': self.object.lesson.pk})
