from datetime import timedelta

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import CreateView, DeleteView

from django.shortcuts import get_object_or_404
from django.urls import reverse

from webapp.models import Period, Group, TYPE_LESSON, TYPE_TASK, TYPE_CONTROL
from webapp.forms import PeriodForm


class PeriodView(PermissionRequiredMixin, DeleteView):
    template_name = 'period/view.html'
    model = Period

    def has_permission(self):
        users = get_object_or_404(Period, pk=self.kwargs.get('pk')).group.users.all()
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in users

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        lessons = self.object.lessons.filter(type_lesson=TYPE_LESSON, materials__type_material=TYPE_TASK).order_by(
            'data_start')
        control = self.object.lessons.filter(type_lesson=TYPE_CONTROL,
                                             materials__type_material=TYPE_TASK).first() or None
        users = self.object.group.users.all()
        results = []
        for user in users:
            works = []
            total = 0
            for lesson in lessons:
                work = lesson.lesson_works.filter(user=user, status=True).first()
                if work:
                    total += work.grade
                works.append(work or None)
            work_control = control.lesson_works.filter(user=user, status=True).first() or None if control else None
            if lessons:
                total = total / lessons.count() * 5

            if work_control:
                total += work_control.grade * 0.5

            results.append({'user': user, 'works': works, 'work_control': work_control, 'total': total})
        context['i_lesson'] = list(range(1, lessons.count() + 1))
        context['results'] = results
        context['lessons_count'] = lessons.count()
        context['control'] = control
        return context


class PeriodCreate(PermissionRequiredMixin, CreateView):
    template_name = 'period/create.html'
    model = Period
    form_class = PeriodForm

    def has_permission(self):
        users = get_object_or_404(Group, pk=self.kwargs.get('group_pk')).users.all()
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in users

    def get_success_url(self):
        return reverse('webapp:group_view_period_pk',
                       kwargs={'period_pk': self.object.pk, 'group_pk': self.object.group.pk})

    def get_initial(self):
        initial = super().get_initial()
        group = get_object_or_404(Group, pk=self.kwargs.get('group_pk'))
        last_period = group.periods.order_by('number_period').last()
        if last_period:
            initial['data_start'] = (last_period.data_end + timedelta(days=1)).strftime('%Y-%m-%d')
        else:
            initial['data_start'] = (group.data_start + timedelta(days=1)).strftime('%Y-%m-%d')
        return initial

    def form_valid(self, form):
        group = get_object_or_404(Group, pk=self.kwargs.get('group_pk'))
        form.instance.group = group
        last_period = group.periods.order_by('number_period').last()
        number_period = 1
        if last_period:
            number_period += last_period.number_period

        form.instance.number_period = number_period

        return super().form_valid(form)


class PeriodDelete(PermissionRequiredMixin, DeleteView):
    model = Period

    def has_permission(self):
        users = get_object_or_404(Period, pk=self.kwargs.get('pk')).group.users.all()
        return self.request.user.is_superuser or self.request.user.is_admin() or self.request.user.is_teacher() and self.request.user in users

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('webapp:group_view', kwargs={'group_pk': self.object.group.pk})
