from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import CreateView, DeleteView

from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404

from webapp.models import Language
from webapp.forms import LanguageForm


class LanguageCreate(PermissionRequiredMixin, CreateView):
    template_name = 'languages/create.html'
    model = Language
    form_class = LanguageForm
    success_url = reverse_lazy('webapp:groups_list')

    def has_permission(self):
        return self.request.user.is_superuser or self.request.user.is_admin()


class LanguageDelete(PermissionRequiredMixin, DeleteView):
    model = Language
    success_url = reverse_lazy('webapp:groups_list')
    pk_url_kwarg = 'language_pk'

    def has_permission(self):
        language = get_object_or_404(Language, pk=self.kwargs.get('language_pk'))
        return not language.groups.all() and (self.request.user.is_superuser or self.request.user.is_admin())

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)
