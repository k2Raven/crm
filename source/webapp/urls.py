from django.urls import path

from webapp.views import IndexView, GroupsList, GroupView, LessonView, MaterialView, MaterialList, \
    PeriodCreate, PeriodDelete, LessonCreate, LessonUpdate, LessonDelete, LessonGroupCreate, LanguageCreate, \
    LanguageDelete, GroupCreate, GroupUpdate, GroupDelete, MaterialCreate, MaterialUpdate, MaterialDelete, \
    MaterialSearchCreate, LinkBroadcastCreate, LinkBroadcastDelete, WorkUserCreate, WorkUserUpdate, WorkUserCheck, \
    LessonUpdateDeadlineTask, WorkLessonCheck, PeriodView, WorkView

app_name = 'webapp'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('lesson/<int:lesson_pk>/', LessonView.as_view(), name='lesson_view'),
    path('lesson/create/', LessonCreate.as_view(), name='lesson_create'),
    path('lesson/<int:group_pk>/create/', LessonGroupCreate.as_view(), name='lesson_group_create'),
    path('lesson/<int:lesson_pk>/update/', LessonUpdate.as_view(), name='lesson_update'),
    path('lesson/<int:lesson_pk>/update/deadline_task/', LessonUpdateDeadlineTask.as_view(),
         name='lesson_update_deadline_task'),
    path('lesson/<int:lesson_pk>/delete/', LessonDelete.as_view(), name='lesson_delete'),

    path('group/', GroupsList.as_view(), name='groups_list'),
    path('group/<int:group_pk>/', GroupView.as_view(), name='group_view'),
    path('group/<int:group_pk>/period/<int:period_pk>/', GroupView.as_view(), name='group_view_period_pk'),
    path('group/<int:language_pk>/create/', GroupCreate.as_view(), name='group_create'),
    path('group/<int:group_pk>/update/', GroupUpdate.as_view(), name='group_update'),
    path('group/<int:group_pk>/delete/', GroupDelete.as_view(), name='group_delete'),

    path('materials/', MaterialList.as_view(), name='materials_list'),
    path('material/<int:material_pk>/', MaterialView.as_view(), name='material_view'),
    path('material/<int:group_pk>/lesson/<int:lesson_pk>/type/<str:type>/create/', MaterialCreate.as_view(),
         name='material_create'),
    path('material/<int:group_pk>/lesson/<int:lesson_pk>/type/<str:type>/create_search/',
         MaterialSearchCreate.as_view(),
         name='material_search_create'),
    path('material/<int:material_pk>/update/', MaterialUpdate.as_view(), name='material_update'),
    path('material/<int:material_pk>/delete/', MaterialDelete.as_view(), name='material_delete'),

    path('language/create/', LanguageCreate.as_view(), name='language_create'),
    path('language/<int:language_pk>/delete/', LanguageDelete.as_view(), name='language_delete'),

    path('period/create/<int:group_pk>/', PeriodCreate.as_view(), name='period_create'),
    path('period/delete/<int:pk>/', PeriodDelete.as_view(), name='period_delete'),
    path('period/<int:pk>/', PeriodView.as_view(), name='period_results'),

    path('work/<int:pk>/', WorkView.as_view(), name='work_view'),
    path('work/lesson/<int:lesson_pk>/create/', WorkUserCreate.as_view(), name='work_create'),
    path('work/<int:work_pk>/update/', WorkUserUpdate.as_view(), name='work_update'),
    path('work/<int:work_pk>/verification/', WorkUserCheck.as_view(), name='work_check'),
    path('work/lesson/<int:lesson_pk>/lesson_check/', WorkLessonCheck.as_view(), name='work_lesson_check'),

    path('link_broadcast/lesson/<int:lesson_pk>/create/', LinkBroadcastCreate.as_view(),
         name='link_broadcast_create'),
    path('link_broadcast/<int:link_broadcast_pk>/delete/', LinkBroadcastDelete.as_view(),
         name='link_broadcast_delete'),
]
