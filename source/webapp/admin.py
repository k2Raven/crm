from django.contrib import admin

from webapp.models import Language, Group, Period, Lesson, LinkBroadcast, Work, GroupUser, Material


class LanguageAdmin(admin.ModelAdmin):
    list_display = ['id', 'name_language']
    list_filter = ['name_language']
    search_fields = ['name_language']
    fields = ['name_language']


admin.site.register(Language, LanguageAdmin)


class GroupAdmin(admin.ModelAdmin):
    list_display = ['id', 'number_group', 'data_start', 'data_end', 'language']
    list_filter = ['data_start', 'number_group']
    search_fields = ['number_group']
    exclude = []


admin.site.register(Group, GroupAdmin)


class PeriodAdmin(admin.ModelAdmin):
    list_display = ['id', 'number_period', 'data_start', 'data_end', 'group']
    list_filter = ['group', 'data_start', 'number_period']
    search_fields = ['number_group']
    exclude = []


admin.site.register(Period, PeriodAdmin)


class LessonAdmin(admin.ModelAdmin):
    list_display = ['id', 'theme', 'data_start', 'type_lesson', 'period']
    list_filter = ['data_start', 'theme']
    search_fields = ['theme']
    exclude = []


admin.site.register(Lesson, LessonAdmin)


class LinkBroadcastAdmin(admin.ModelAdmin):
    list_display = ['id', 'link']
    exclude = []


admin.site.register(LinkBroadcast, LinkBroadcastAdmin)


class WorkAdmin(admin.ModelAdmin):
    list_display = ['id', 'lesson', 'user', 'status']
    list_filter = ['lesson', 'status']
    exclude = []


admin.site.register(Work, WorkAdmin)


class GroupUserAdmin(admin.ModelAdmin):
    list_display = ['id', 'group', 'user', 'data_start', 'data_end']
    list_filter = ['data_start', 'data_end']
    exclude = []


admin.site.register(GroupUser, GroupUserAdmin)

admin.site.register(Material)
