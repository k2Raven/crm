from django.urls import path, re_path

from accounts.views import RegisterView, UserDetailView, UserChangeView, UserPasswordChangeView, UserList, UserAdd, \
    TokenDelete, UserResults
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView

app_name = 'accounts'

urlpatterns = [
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('create/<str:token>/', RegisterView.as_view(), name='create'),
    path('<int:pk>/', UserDetailView.as_view(), name='detail'),
    path('change/', UserChangeView.as_view(), name='change'),
    path('user/add/', UserAdd.as_view(), name='user_add'),
    path('user/group/<int:group_pk>/results/', UserResults.as_view(), name='user_results'),
    path('user/group/<int:group_pk>/period/<int:period_pk>/results/', UserResults.as_view(), name='user_results_period'),
    path('token/<int:pk>/delete/', TokenDelete.as_view(), name='token_delete'),
    path('password_change/', UserPasswordChangeView.as_view(), name='password_change'),
    path("password_reset/", PasswordResetView.as_view(), name="password_reset"),
    re_path(r'^users/(?P<is_filter>registered|not_registered)/$', UserList.as_view(), name='users_list_is_filter'),
]
