from django.db import models
from django.contrib.auth.models import AbstractUser

from secrets import token_urlsafe

from django.db.models import Avg
from math import ceil

ROLE_TEACHER = 'teacher'
ROLE_ADMIN = 'admin'
ROLE_STUDENT = 'student'
ROLES_USER = [
    (ROLE_TEACHER, 'Учитель'),
    (ROLE_ADMIN, 'Администратор'),
    (ROLE_STUDENT, 'Студент'),
]


class RoleAndGroup(models.Model):
    token = models.CharField(max_length=50, unique=True, default=token_urlsafe)
    created_at = models.DateTimeField(auto_now_add=True)
    is_activation = models.BooleanField(default=False)
    groups = models.ManyToManyField('webapp.Group', related_name='role_and_group', blank=True, verbose_name="Группы")
    user_role = models.CharField(choices=ROLES_USER, max_length=20, verbose_name='Тип')
    data_start = models.DateField(verbose_name='Дата начала учебы')

    def __str__(self):
        return f"{self.token} {self.user_role} {self.is_activation} {self.created_at}"


class User(AbstractUser):
    birth_date = models.DateField(null=True, verbose_name='Дата рождения')
    avatar = models.ImageField(null=True, blank=True, verbose_name='Фотография')
    role_and_group = models.OneToOneField('accounts.RoleAndGroup', related_name='user', on_delete=models.PROTECT)

    def get_total_results(self, period_pk):
        total = self.user_works.filter(lesson__period_id=period_pk).exclude(lesson__type_lesson='control').aggregate(
            total=Avg('grade'))['total'] * 5
        work_control = self.user_works.filter(lesson__period_id=period_pk, lesson__type_lesson='control').first()
        if work_control:
            total += work_control.grade
        return ceil(total)

    def is_admin(self):
        return self.role_and_group.user_role == ROLE_ADMIN

    def is_teacher(self):
        return self.role_and_group.user_role == ROLE_TEACHER

    def is_student(self):
        return self.role_and_group.user_role == ROLE_STUDENT

    def get_groups(self):
        return self.role_and_group.groups.all()
