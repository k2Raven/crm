# Generated by Django 4.2.1 on 2023-06-13 13:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0025_group_is_activation'),
        ('accounts', '0004_alter_roleandgroup_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='roleandgroup',
            name='groups',
            field=models.ManyToManyField(blank=True, related_name='groups', to='webapp.group', verbose_name='Группы'),
        ),
    ]
