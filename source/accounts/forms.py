from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.forms import widgets
from accounts.models import RoleAndGroup
from webapp.models import Group


class RoleAndGroupForm(forms.ModelForm):
    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.filter(is_activation=True), required=False,
                                            label="Группы",
                                            widget=forms.CheckboxSelectMultiple(attrs={'class': 'mb-2', }))

    class Meta:
        model = RoleAndGroup
        fields = ['user_role', 'groups', 'data_start']
        widgets = {
            'user_role': widgets.Select(attrs={
                'class': 'form-select w-50'
            }),
            'data_start': forms.DateInput(attrs={
                'type': 'date',
                'class': 'form-control w-50'
            }, format='%Y-%m-%d')
        }


class MyUserCreationForm(UserCreationForm):
    password1 = forms.CharField(label="Пароль", strip=False,
                                widget=forms.PasswordInput(attrs={'class': 'form-control w-50'}))
    password2 = forms.CharField(label="Подтверждение пароля",
                                widget=forms.PasswordInput(attrs={'class': 'form-control w-50'}), strip=False)

    class Meta(UserCreationForm.Meta):
        model = get_user_model()
        fields = ['username', 'password1', 'password2', 'first_name', 'last_name', 'email', 'birth_date']
        widgets = {
            'username': widgets.TextInput(attrs={
                'class': 'form-control w-50'
            }),
            'first_name': widgets.TextInput(attrs={
                'class': 'form-control w-50'
            }),
            'last_name': widgets.TextInput(attrs={
                'class': 'form-control w-50'
            }),
            'email': widgets.EmailInput(attrs={
                'class': 'form-control w-50'
            }),
            'birth_date': widgets.DateInput(attrs={
                'type': 'date',
                'class': 'form-control w-50'
            }, format='%Y-%m-%d'),
            'avatar': widgets.FileInput(attrs={
                'class': 'form-control rounded-pill mb-3 w-50'
            })
        }


class UserChangeForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name', 'email', 'birth_date', 'avatar']
        labels = {'first_name': 'Имя', 'last_name': 'Фамилия', 'email': 'Email'}
        widgets = {
            'first_name': widgets.TextInput(attrs={
                'class': 'form-control w-50'
            }),
            'last_name': widgets.TextInput(attrs={
                'class': 'form-control w-50'
            }),
            'email': widgets.EmailInput(attrs={
                'class': 'form-control w-50'
            }),
            'birth_date': widgets.DateInput(attrs={
                'type': 'date',
                'class': 'form-control w-50'
            }, format='%Y-%m-%d'),
            'avatar': widgets.FileInput(attrs={
                'class': 'form-control rounded-pill mb-3 w-50'
            })
        }


class MyPasswordChangeForm(PasswordChangeForm):
    new_password1 = forms.CharField(label="Новый пароль", strip=False,
                                    widget=forms.PasswordInput(attrs={'class': 'form-control w-50'}))
    new_password2 = forms.CharField(label="Подтвердите пароль",
                                    widget=forms.PasswordInput(attrs={'class': 'form-control w-50'}), strip=False)
    old_password = forms.CharField(label="Старый пароль", strip=False,
                                   widget=forms.PasswordInput(attrs={'class': 'form-control w-50'}))

    class Meta:
        model = get_user_model()
