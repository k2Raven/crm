from django.contrib.auth import get_user_model, login
from django.contrib.auth.views import PasswordChangeView
from django.shortcuts import redirect, get_object_or_404, Http404
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DetailView, UpdateView, ListView, DeleteView, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from accounts.forms import MyUserCreationForm, UserChangeForm, MyPasswordChangeForm, RoleAndGroupForm
from accounts.models import RoleAndGroup
from webapp.models import GroupUser, Period, Group, TYPE_LESSON, TYPE_CONTROL, TYPE_TASK
from django.db.models import Sum


class UserList(PermissionRequiredMixin, ListView):
    model = RoleAndGroup
    template_name = 'user_list.html'
    context_object_name = 'role_and_group'
    ordering = '-created_at'

    def has_permission(self):
        return self.request.user.is_superuser or self.request.user.is_admin()

    def get_queryset(self):
        queryset = super().get_queryset()
        is_filter = self.kwargs.get('is_filter', None)
        if is_filter:
            is_activ = True if is_filter == 'registered' else False
            queryset = queryset.filter(is_activation=is_activ)
        return queryset


class UserAdd(PermissionRequiredMixin, CreateView):
    model = RoleAndGroup
    form_class = RoleAndGroupForm
    template_name = 'user_add.html'
    success_url = reverse_lazy('accounts:users_list_is_filter', kwargs={'is_filter': 'not_registered'})

    def has_permission(self):
        return self.request.user.is_superuser or self.request.user.is_admin()


class TokenDelete(PermissionRequiredMixin, DeleteView):
    model = RoleAndGroup
    success_url = reverse_lazy('accounts:users_list_is_filter', kwargs={'is_filter': 'not_registered'})

    def get_queryset(self):
        return RoleAndGroup.objects.filter(is_activation=False)

    def has_permission(self):
        return self.request.user.is_superuser or self.request.user.is_admin()

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)


class UserResults(PermissionRequiredMixin, TemplateView):
    template_name = 'user_results.html'

    def has_permission(self):
        return self.request.user.is_student()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        group_pk = self.kwargs.get('group_pk')
        group = get_object_or_404(Group, pk=group_pk)
        context['group'] = group
        period_pk = self.kwargs.get('period_pk')
        if period_pk:
            period = group.periods.get(pk=period_pk)
        else:
            period = group.periods.get(number_period=1)
        context['period_now'] = period

        lessons = period.lessons.filter(type_lesson=TYPE_LESSON, materials__type_material=TYPE_TASK).order_by(
            'data_start')
        control = period.lessons.filter(type_lesson=TYPE_CONTROL, materials__type_material=TYPE_TASK).first()
        context['lessons'] = lessons
        context['control'] = control
        lessons_count = lessons.count()

        total_result = 0
        if lessons:
            print(self.request.user.user_works.filter(lesson__in=lessons, status=True).aggregate(total=Sum('grade')))
            total_result = \
                self.request.user.user_works.filter(lesson__in=lessons, status=True).aggregate(total=Sum('grade'))[
                    'total'] or 0 / lessons_count * 5
            if control:
                control_user = control.lesson_works.filter(user=self.request.user).first()
                lessons_count += 1
                if control_user:
                    total_result = total_result + control_user.grade * 0.5
        context['total_result'] = total_result
        context['lessons_count'] = lessons_count
        context['user_works'] = self.request.user.user_works.filter(lesson__period=period, status=True)

        return context


class RegisterView(CreateView):
    model = get_user_model()
    form_class = MyUserCreationForm
    template_name = 'user_create.html'

    def dispatch(self, request, *args, **kwargs):
        token = self.kwargs.get('token')
        self.role_and_group = get_object_or_404(RoleAndGroup, token=token)
        if self.role_and_group.is_activation:
            raise Http404()
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.role_and_group = self.role_and_group
        user = form.save()
        for group in self.role_and_group.groups.all():
            GroupUser.objects.create(user=user, group=group, data_start=self.role_and_group.data_start)
        self.role_and_group.is_activation = True
        self.role_and_group.save()
        login(self.request, user)
        return redirect(self.get_success_url())

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url

        next_url = self.request.POST.get('next')
        if next_url:
            return next_url

        return reverse('webapp:index')


class UserDetailView(PermissionRequiredMixin, DetailView):
    model = get_user_model()
    template_name = 'user_detail.html'
    context_object_name = 'user_obj'

    def has_permission(self):
        return self.request.user.is_superuser or self.request.user.is_admin() \
            or self.request.user.is_teacher() or self.request.user == self.object

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['groups'] = self.object.groups_language.all()
        return context


class UserChangeView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    form_class = UserChangeForm
    template_name = 'user_change.html'
    context_object_name = 'user_obj'

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('accounts:detail', kwargs={'pk': self.get_object().pk})


class UserPasswordChangeView(PasswordChangeView):
    template_name = 'user_password_change.html'
    form_class = MyPasswordChangeForm

    def get_success_url(self):
        return reverse('accounts:detail', kwargs={'pk': self.request.user.pk})
