from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model
from accounts.models import RoleAndGroup

User = get_user_model()


class MyUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + ((None, {"fields": ['birth_date', 'avatar', 'role_and_group']}),)
    add_fieldsets = UserAdmin.add_fieldsets + ((None, {"fields": ['birth_date', 'avatar', 'role_and_group']}),)


admin.site.register(User, MyUserAdmin)

admin.site.register(RoleAndGroup)
